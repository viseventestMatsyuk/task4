if (localStorage.getItem('mainWrapper')) {
    main_wrapper.innerHTML = localStorage.getItem('mainWrapper');
};
window.onbeforeunload = function(e) {
    const main_wrapper = document.getElementById('main_wrapper');
    const create = main_wrapper.innerHTML;
    localStorage.setItem('mainWrapper', create);
}